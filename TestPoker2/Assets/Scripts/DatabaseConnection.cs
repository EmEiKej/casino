using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using UnityEngine;
public class DatabaseConnection : MonoBehaviour
{
    string server = "pokerserver.database.windows.net";
    string database = "Pokerdatabase";
    string username = "EmEiKej";
    string password = "Poker2024";
    string connectionString;
    void Start()
    {
        connectionString = $"Server={server};Database={database};User Id={username};Password={password};";
    }
    void ConnectionSchema(string query)
    {
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Error: {ex.Message}");
            }
            finally
            {
                connection.Close();
            }
        }
    }
    public void CreateRoom(string roomName, string roomPassword, string playerName)
    {
        string query = $"INSERT INTO Rooms (Name, Password, IsRunning) VALUES ('{roomName}', '{roomPassword}','false')";
        ConnectionSchema(query);
        CreateTable(roomName);
        AddPlayer(roomName, playerName, true);
    }
    void CreateTable(string roomName)
    {
        string query = $"Create Table {roomName} (PlayerName NVARCHAR(255),IsHost BIT,IsReady BIT)";
        ConnectionSchema(query);
    }
    void AddPlayer(string roomName, string playerName, bool isHost)
    {
        string query = $"INSERT INTO {roomName} (PlayerName,IsHost,IsReady) VALUES ('{playerName}','{isHost}','false')";
        ConnectionSchema(query);
    }
    public void DropDeck(string roomName)
    {
        string query = $"Drop Table {roomName + "Deck"}";
        ConnectionSchema(query);
    }
    public void DropTurn(string roomName)
    {
        string query = $"Drop Table {roomName + "Turn"}";
        ConnectionSchema(query);
    }
    public void RoomIsRunning(string roomName)
    {
        string query = $"UPDATE Rooms SET IsRunning = 'true' WHERE Name = '{roomName}';";
        ConnectionSchema(query);
    }
    public void removePlayer(string roomName, string playerName)
    {
        string query = $"DELETE FROM {roomName} WHERE PlayerName = '{playerName}';";
        ConnectionSchema(query);
    }
    public void PlayerReadyState(string roomName, string playerName, bool isReady)
    {
        string query = $"UPDATE {roomName} SET IsReady = '{isReady}' WHERE PlayerName = '{playerName}';";
        ConnectionSchema(query);
    }
    public void SendMove(string roomName, string playerName, string chosenAction, string betValue)
    {
        string query = $"IF OBJECT_ID('{roomName + "Turn"}', 'U') IS NOT NULL BEGIN DROP TABLE {roomName + "Turn"}; END";
        string query1 = $"Create Table {roomName + "Turn"} (PlayerName NVARCHAR(255),ChosenAction NVARCHAR(255),BetValue NVARCHAR(255));";
        string query2 = $"INSERT INTO {roomName + "Turn"} (PlayerName,ChosenAction,BetValue) VALUES ('{playerName}', '{chosenAction}','{betValue}')";
        ConnectionSchema(query);
        ConnectionSchema(query1);
        ConnectionSchema(query2);
    }
    public void SendDeck(string roomName, List<string> types, List<string> values)
    {
        string query = $"Create Table {roomName + "Deck"} (Type NVARCHAR(255),Value NVARCHAR(255));";
        ConnectionSchema(query);
        for (int i = 0; i < types.Count; i++)
        {
            string query1 = $"INSERT INTO {roomName + "Deck"} (Type,Value) VALUES ('{types[i]}', '{values[i]}')";
            ConnectionSchema(query1);
        }
    }
    public void removeRoom(string roomName)
    {
        string query = $"Drop Table {roomName}";
        string query1 = $"Drop Table {roomName + "Deck"}";
        string query2 = $"Drop Table {roomName + "Turn"}";
        string query3 = $"DELETE FROM Rooms WHERE name = '{roomName}';";
        ConnectionSchema(query);
        ConnectionSchema(query1);
        ConnectionSchema(query2);
        ConnectionSchema(query3);
    }
    public List<string> GetMove(string roomName)
    {
        List<string> values = new List<string>();
        string selectTable = $"IF OBJECT_ID('{roomName + "Turn"}','U') IS NOT NULL BEGIN SELECT * FROM {roomName + "Turn"}; END";
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(selectTable, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                values.Add(reader.GetString(0));
                                values.Add(reader.GetString(1));
                                values.Add(reader.GetString(2));
                            }
                        }
                        else
                        {
                            Debug.Log("No rows found.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Error: {ex.Message}");
            }
            finally
            {
                connection.Close();
            }
            return values;
        }
    }
    public (List<string>, List<string>) GetDeck(string roomName)
    {
        List<string> types = new List<string>();
        List<string> values = new List<string>();
        string selectTable = $"SELECT * FROM {roomName + "Deck"}";
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(selectTable, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                types.Add(reader.GetString(0));
                                values.Add(password = reader.GetString(1));
                            }
                        }
                        else
                        {
                            Debug.Log("No rows found.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Error: {ex.Message}");
            }
            finally
            {
                connection.Close();
            }
            return (types, values);
        }
    }
    public bool JoinRoom(string roomName, string roomPassword, string playerName)
    {
        string selectTable = $"SELECT * FROM Rooms";
        string name = string.Empty;
        string password = string.Empty;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(selectTable, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                name = reader.GetString(0);
                                password = reader.GetString(1);
                                if (name == roomName && password == roomPassword)
                                {
                                    AddPlayer(roomName, playerName, false);
                                    return true;
                                }
                            }
                        }
                        else
                        {
                            Debug.Log("No rows found.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Error: {ex.Message}");
            }
            finally
            {
                connection.Close();
            }
            return false;
        }
    }
    public List<string> GetPlayersNames(string roomName)
    {
        List<string> names = new List<string>();
        string selectTable = $"SELECT PlayerName FROM {roomName}";
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(selectTable, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                names.Add(reader.GetString(0));
                            }
                        }
                        else
                        {
                            Debug.Log("No rows found.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Error: {ex.Message}");
            }
            finally
            {
                connection.Close();
            }
            return names;
        }
    }
    public List<string> GetRoomNames()
    {
        List<string> names = new List<string>();
        string selectTable = $"SELECT Name FROM Rooms";
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(selectTable, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                names.Add(reader.GetString(0));
                            }
                        }
                        else
                        {
                            Debug.Log("No rows found.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Error: {ex.Message}");
            }
            finally
            {
                connection.Close();
            }
            return names;
        }
    }
    public List<bool> GetPlayersState(string roomName)
    {
        List<bool> states = new List<bool>();
        string selectTable = $"SELECT IsReady FROM {roomName}";
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(selectTable, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                states.Add(reader.GetBoolean(0));
                            }
                        }
                        else
                        {
                            Debug.Log("No rows found.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Error: {ex.Message}");
            }
            finally
            {
                connection.Close();
            }
            return states;
        }
    }
    public bool CheckIfRoomIsRunnning(string roomName)
    {
        string selectTable = $"SELECT IsRunning FROM Rooms WHERE Name = '{roomName}'";
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(selectTable, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                return reader.GetBoolean(0);
                            }
                        }
                        else
                        {
                            Debug.Log("No rows found.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Error: {ex.Message}");
            }
            finally
            {
                connection.Close();
            }
            return true;
        }
    }
}