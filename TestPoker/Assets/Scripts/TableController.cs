using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class TableController : MonoBehaviour
{
    [SerializeField] List<GameObject> cardsPrefab;
    [SerializeField] Menu menu;
    public List<Card> deck = new List<Card>();
    List<Player> activePlayers;
    List<Card> tableCards = new List<Card>();
    List<Card> stosCards = new List<Card>();
    [SerializeField] PokerController pokerController;
    [SerializeField] DatabaseConnection databaseConnection;
    [SerializeField] Lobby lobby;
    [SerializeField] Transform cardsSpot;
    [SerializeField] GameObject actionWindow;
    [SerializeField] Button call;
    [SerializeField] Button bet;
    [SerializeField] Button check;
    [SerializeField] Slider betSlider;
    [SerializeField] TMP_Text betText;
    [SerializeField] TMP_Text potText;
    [SerializeField] List<Transform> tableCardsPositions;
    [SerializeField] Transform stosCardsPosition;
    [SerializeField] GameObject winnerPrefab;
    [SerializeField] TMP_Text winnerText;
    int roundNumer = 0;
    bool stopEnteringMyTurn = false;
    bool stopEnteringOpponentTurn = false;
    Player mySelf;
    int dealer = 0;
    int pul = 0;
    bool isWaitingForDeck = false;
    int smallBlind = 10;
    int bigBlind = 20;
    string playerOnMove = string.Empty;
    int playerOnMoveIndex;
    int actionsMade = 0;
    int actionsToMake;
    bool gameBegan = false;
    private void Start()
    {
        StartRound();
    }
    void StartRound()
    {
        GetPlayers();
        actionsToMake = activePlayers.Count;
        playerOnMoveIndex = (dealer + 3) % activePlayers.Count;
        GiveToken();
        GetCards();
        gameBegan = true;

        void GetPlayers()
        {
            activePlayers = new List<Player>(pokerController.players);
            foreach (Player player in activePlayers)
            {
                if (player.Name == pokerController.myNickname)
                {
                    mySelf = player;
                }
            }
        }
        void GetCards()
        {
            if (pokerController.isHost)
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 13 * i; j < 13 * i + 13; j++)
                    {
                        GameObject gameObject = Instantiate(cardsPrefab[j], this.transform);
                        gameObject.transform.position = new Vector3(cardsSpot.position.x, cardsSpot.position.y, -1);
                        deck.Add(gameObject.GetComponent<Card>());
                        deck.Last().AddValues((EType)i, (j % 13) + 1);
                    }
                }
                ShuffleCards();
                SentDeck();
                DealCards();
            }
            else
            {
                isWaitingForDeck = true;
            }
            void ShuffleCards()
            {
                int random;
                List<Card> tempt = new List<Card>();
                for (int i = 0; i < 52; i++)
                {
                    random = UnityEngine.Random.Range(0, deck.Count);
                    tempt.Add(deck[random]);
                    deck.RemoveAt(random);
                }
                deck = tempt;
            }
            void SentDeck()
            {
                databaseConnection.DropDeck(pokerController.roomID);
                List<string> types = new List<string>();
                List<string> values = new List<string>();
                foreach (Card card in deck)
                {
                    types.Add(card.type.ToString());
                    values.Add(card.numer.ToString());
                }
                databaseConnection.SendDeck(pokerController.roomID, types, values);
            }
        }
    }
    void GiveToken()
    {
        for (int i = 0; i < activePlayers.Count; i++)
        {
            if (i == dealer)
            {
                activePlayers[i].token.SetActive(true);
                activePlayers[i].token.GetComponent<Image>().sprite = pokerController.Tokens[0];
                activePlayers[(i + 1) % activePlayers.Count].token.SetActive(true);
                activePlayers[(i + 1) % activePlayers.Count].token.GetComponent<Image>().sprite = pokerController.Tokens[1];
                activePlayers[(i + 2) % activePlayers.Count].token.SetActive(true);
                activePlayers[(i + 2) % activePlayers.Count].token.GetComponent<Image>().sprite = pokerController.Tokens[2];
                if (roundNumer == 0)
                {
                    activePlayers[(i + 1) % activePlayers.Count].AddMoneyToPul(smallBlind);
                    activePlayers[(i + 2) % activePlayers.Count].AddMoneyToPul(bigBlind);
                }
            }
            else if (i != (dealer + 1) % activePlayers.Count && i != (dealer + 2) % activePlayers.Count)
            {
                activePlayers[i].token.SetActive(false);
            }
        }
        playerOnMove = activePlayers[playerOnMoveIndex].Name;
        activePlayers[playerOnMoveIndex].nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.green;
        dealer = (dealer + 1) % activePlayers.Count;
    }
    void DealCards()
    {
        foreach (Player activePlayer in activePlayers)
        {
            activePlayer.GetHand(deck.GetRange(0, 2));
            deck.RemoveRange(0, 2);
            activePlayer.hand[0].gameObject.transform.position = new Vector3(activePlayer.myCardSpot1.position.x, activePlayer.myCardSpot1.position.y, -1);
            activePlayer.hand[1].gameObject.transform.position = new Vector3(activePlayer.myCardSpot2.position.x, activePlayer.myCardSpot2.position.y, -1);
            if (activePlayer.Name == pokerController.myNickname)
            {
                activePlayer.hand[0].gameObject.transform.rotation = Quaternion.Euler(new Vector3(90f, 180f, 0f));
                activePlayer.hand[1].gameObject.transform.rotation = Quaternion.Euler(new Vector3(90f, 180f, 0f));
            }
        }
        gameBegan = true;
    }
    private void Update()
    {
        if (isWaitingForDeck)
        {
            GetDeck();
        }
        if (gameBegan)
        {
            if (playerOnMove != pokerController.myNickname)
            {
                OpponentMove();
            }
            else
            {
                MyMove();
            }
        }
        void GetDeck()
        {
            var lists = databaseConnection.GetDeck(pokerController.roomID);
            if (lists.Item1.Count != 0)
            {
                isWaitingForDeck = false;
                for (int i = 0; i < lists.Item1.Count; i++)
                {
                    int multiplyer = FindMultiplyer(lists.Item1[i]);
                    GameObject gameObject = Instantiate(cardsPrefab[multiplyer + int.Parse(lists.Item2[i]) - 1], this.transform);
                    gameObject.transform.position = new Vector3(cardsSpot.position.x, cardsSpot.position.y, -1);
                    deck.Add(gameObject.GetComponent<Card>());
                    deck.Last().AddValues((EType)Enum.Parse(typeof(EType), lists.Item1[i], true), int.Parse(lists.Item2[i]) % 13);
                }
                DealCards();
            }
            int FindMultiplyer(string CardType)
            {
                if ((EType)Enum.Parse(typeof(EType), CardType, true) == EType.Club)
                {
                    return 0;
                }
                else if ((EType)Enum.Parse(typeof(EType), CardType, true) == EType.Diamond)
                {
                    return 13;
                }
                else if ((EType)Enum.Parse(typeof(EType), CardType, true) == EType.Heart)
                {
                    return 26;
                }
                else
                {
                    return 39;
                }
            }
        }
    }
    void MyMove()
    {
        if (!stopEnteringMyTurn)
        {
            stopEnteringMyTurn = true;
            if (mySelf.money != 0)
            {
                mySelf.nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.green;
                actionWindow.SetActive(true);
                CheckPossibleActions();
            }
            else
            {
                mySelf.nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.cyan;
                activePlayers.RemoveAt(playerOnMoveIndex);
                playerOnMoveIndex = playerOnMoveIndex % activePlayers.Count;
                LookForEndOfRound();
                stopEnteringMyTurn = false;
            }
        }
        void CheckPossibleActions()
        {
            int toCall = ToCall();
            if (toCall > mySelf.pol)
            {
                check.interactable = false;
                if (toCall - mySelf.pol >= mySelf.money)
                {
                    bet.interactable = false;
                    betSlider.interactable = false;
                }
                else
                {
                    betSlider.minValue = toCall + 1 - mySelf.pol;
                    betSlider.maxValue = mySelf.money;
                }
            }
            else if (toCall == mySelf.pol)
            {
                call.interactable = false;
                betSlider.maxValue = mySelf.money;
            }
        }
    }
    int ToCall()
    {
        int toCall = 0;
        foreach (Player player in activePlayers)
        {
            if (player.pol > toCall)
            {
                toCall = player.pol;
            }
        }
        return toCall;
    }
    void OpponentMove()
    {
        if (activePlayers[playerOnMoveIndex].money != 0)
        {
            CheckIfPlayerMadeMove();
        }
        else
        {
            activePlayers[playerOnMoveIndex].nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.cyan;
            activePlayers.RemoveAt(playerOnMoveIndex);
            playerOnMoveIndex = playerOnMoveIndex % activePlayers.Count;
            LookForEndOfRound();
        }
    }
    void CheckIfPlayerMadeMove()
    {
        List<string> values = new List<string>();
        values = databaseConnection.GetMove(pokerController.roomID);
        if (values.Count != 0)
        {
            if (values[0] == playerOnMove)
            {
                if (!stopEnteringOpponentTurn)
                {
                    InterprateMove(values[1], values[2]);
                    LookForEndOfRound();
                    stopEnteringOpponentTurn = false;
                }
            }
        }
        void InterprateMove(string move, string value)
        {
            stopEnteringOpponentTurn = true;
            activePlayers[playerOnMoveIndex].nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.yellow;
            if (move == "Pass")
            {
                activePlayers[playerOnMoveIndex].nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.gray;
                activePlayers.RemoveAt(playerOnMoveIndex);
                playerOnMoveIndex = playerOnMoveIndex % activePlayers.Count;
            }
            else if (move == "Check")
            {
                playerOnMoveIndex = (playerOnMoveIndex + 1) % activePlayers.Count;
            }
            else if (move == "Call")
            {
                activePlayers[playerOnMoveIndex].AddMoneyToPul(int.Parse(value));
                playerOnMoveIndex = (playerOnMoveIndex + 1) % activePlayers.Count;
            }
            else if (move == "Bet")
            {
                actionsMade = 0;
                actionsToMake = activePlayers.Count;
                activePlayers[playerOnMoveIndex].AddMoneyToPul(int.Parse(value));
                playerOnMoveIndex = (playerOnMoveIndex + 1) % activePlayers.Count;
            }
        }
    }
    void LookForEndOfRound()
    {
        playerOnMove = activePlayers[playerOnMoveIndex].Name;
        actionsMade++;
        if (CheckIfEndOfRound())
        {
            FinishRound();
        }
        else
        {
            activePlayers[playerOnMoveIndex].nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.green;
        }
        bool CheckIfEndOfRound()
        {
            bool somePlayerHasMoney = false;
            bool potsAreSame = true;
            int playerWithMoney = 0;
            if (actionsMade >= actionsToMake)
            {
                for (int i = 0; i < activePlayers.Count; i++)
                {
                    if (activePlayers[i].money != 0)
                    {
                        somePlayerHasMoney = true;
                        playerWithMoney = i;
                    }
                }
                if (!somePlayerHasMoney)
                {
                    return true;
                }
                else
                {
                    foreach (Player player in activePlayers)
                    {
                        if (activePlayers[playerWithMoney].pol != player.pol)
                        {
                            if (player.money != 0)
                            {
                                potsAreSame = false;
                            }
                        }
                    }
                    if (potsAreSame)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
    void FinishRound()
    {
        if (roundNumer == 3)
        {
            foreach (Player player in activePlayers)
            {
                if (player.Name != pokerController.myNickname)
                {
                    player.RotateCards();
                }
            }
            stopEnteringMyTurn = false;
            stopEnteringOpponentTurn = false;
            foreach (Player player in pokerController.players)
            {
                pul += player.pol;
                player.AddPolToRoundPol();
            }
            potText.text = pul.ToString();
            roundNumer = 0;
            actionsMade = 0;
            Player winner = FindWinners();
            winner.nameKeeper.GetComponent<SpriteRenderer>().color = new UnityEngine.Color(255, 215, 0);
            winner.money += pul;
            winner.moneyLabel.text = winner.money.ToString();
            pul = 0;
            potText.text = pul.ToString();
            if (IsGameOver())
            {
                StartCoroutine(DeleyGameOver());
            }
            else
            {
                StartCoroutine(DeleyStartingNewRound(winner));
            }
        }
        else if (roundNumer == 2)
        {
            PlaceCardsOnTable(1);
            StandardRoundActions();
        }
        else if (roundNumer == 1)
        {
            PlaceCardsOnTable(1);
            StandardRoundActions();
        }
        else if (roundNumer == 0)
        {
            PlaceCardsOnTable(3);
            StandardRoundActions();
        }
        IEnumerator DeleyGameOver()
        {
            yield return new WaitForSeconds(3f);
            winnerPrefab.SetActive(true);
            foreach (Player player in pokerController.players)
            {
                if (player.money > 0)
                {
                    winnerText.text = player.Name;
                }
            }
            lobby.gameObject.SetActive(false);
        }
            IEnumerator DeleyStartingNewRound(Player winner)
        {
            yield return new WaitForSeconds(3f);
            DestroyCards();
            StartRound();
            void DestroyCards()
            {
                foreach (Card card in stosCards)
                {
                    Destroy(card.gameObject);
                }
                stosCards.Clear();
                foreach (Card card in deck)
                {
                    Destroy(card.gameObject);
                }
                deck.Clear();
                foreach (Card card in tableCards)
                {
                    Destroy(card.gameObject);
                }
                tableCards.Clear();
                foreach (Player player in pokerController.players)
                {
                    player.PrepareDataForNewRound();
                }
            }
            winner.nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.yellow;
        }
        Player FindWinners()
        {
            List<Player> potencialWinners = new List<Player>();
            List<Player> winners = new List<Player>();
            potencialWinners = FindPotencialWinners();
            FindBestResults();
            List<Player> sortedPotencialWinners = potencialWinners.OrderByDescending(card => card.bestResult).ToList();
            return sortedPotencialWinners[0];
            List<Player> FindPotencialWinners()
            {
                List<Player> potencialWinners = new List<Player>();
                foreach (Player player in pokerController.players)
                {
                    if (activePlayers.Contains(player))
                    {
                        potencialWinners.Add(player);
                    }
                    else if (player.money == 0)
                    {
                        potencialWinners.Add(player);
                    }
                }
                return potencialWinners;
            }
            void FindBestResults()
            {
                foreach (Player potencialWinner in potencialWinners)
                {
                    List<Card> sevenCards = new List<Card>();
                    sevenCards.AddRange(potencialWinner.hand);
                    sevenCards.AddRange(tableCards);
                    var sortedSevenCardsByNumer = sevenCards.OrderBy(card => card.numer).ToList();
                    var sortedSevenCardsByType = sevenCards.OrderBy(card => card.type).ThenBy(card => card.numer).ToList();
                    bool pair = false;
                    bool twoPairs = false;
                    bool threeOfKind = false;
                    bool straight = false;
                    bool flush = false;
                    bool fullHouse = false;
                    bool fourOfKind = false;
                    bool poker = false;
                    int flushCounter = 1;
                    int straightCounter = 1;
                    int pokerCounter = 0;
                    int pairIndex = 0;
                    int twoParisIndex1 = 0;
                    int twoParisIndex2 = 0;
                    int threeOfKindIndex = 0;
                    int straightIndex = 0;
                    int flushIndex = 0;
                    int fullHouesIndex1 = 0;
                    int fullHouesIndex2 = 0;
                    int fourOfKindIndex = 0;
                    int pokerIndex = 0;
                    for (int i = 0; i < 6; i++)
                    {
                        if (sortedSevenCardsByType[i].type == sortedSevenCardsByType[i + 1].type)
                        {
                            flushCounter++;
                            if (flushCounter >= 5)
                            {
                                flush = true;
                                flushIndex = i + 1;
                            }
                            if (sortedSevenCardsByType[i].numer == sortedSevenCardsByType[i + 1].numer - 1)
                            {
                                pokerCounter++;
                                if (pokerCounter >= 5)
                                {
                                    poker = true;
                                    pokerIndex = i + 1;
                                }
                            }
                            else
                            {
                                pokerCounter = 1;
                            }
                        }
                        else
                        {
                            flushCounter = 1;
                            pokerCounter = 1;
                        }
                    }
                    for (int i = 0; i < 6; i++)
                    {
                        if (sortedSevenCardsByNumer[i].numer == sortedSevenCardsByNumer[i + 1].numer)
                        {
                            if (pair)
                            {
                                if (sortedSevenCardsByNumer[i - 1].numer == sortedSevenCardsByNumer[i].numer)
                                {
                                    if (threeOfKind)
                                    {
                                        if (sortedSevenCardsByNumer[i - 2].numer == sortedSevenCardsByNumer[i - 1].numer)
                                        {
                                            fourOfKind = true;
                                            fourOfKindIndex = i - 2;
                                        }
                                    }
                                    else
                                    {
                                        threeOfKind = true;
                                        threeOfKindIndex = i - 1;
                                    }
                                }
                                else
                                {
                                    twoPairs = true;
                                    twoParisIndex1 = pairIndex;
                                    twoParisIndex2 = i;
                                }
                            }
                            else
                            {
                                pair = true;
                                pairIndex = i;
                            }
                        }
                        else if (sortedSevenCardsByNumer[i].numer == sortedSevenCardsByNumer[i + 1].numer - 1)
                        {
                            straightCounter++;
                            if (straightCounter >= 5)
                            {
                                straight = true;
                                straightIndex = i + 1;
                            }
                        }
                        else if (sortedSevenCardsByNumer[i].numer != sortedSevenCardsByNumer[i + 1].numer - 1)
                        {
                            straightCounter = 1;
                        }
                    }
                    if (twoPairs && threeOfKind)
                    {
                        fullHouse = true;
                        fullHouesIndex1 = threeOfKindIndex;
                        if (sortedSevenCardsByNumer[threeOfKindIndex].numer == sortedSevenCardsByNumer[twoParisIndex1].numer)
                        {
                            fullHouesIndex2 = twoParisIndex2;
                        }
                        else
                        {
                            fullHouesIndex2 = twoParisIndex1;
                        }
                    }
                    if (poker)
                    {
                        potencialWinner.bestResult = long.Parse("9" + (sortedSevenCardsByType[pokerIndex].numer).ToString("D2") + "00000000");
                    }
                    else if (fourOfKind)
                    {
                        int counter = 0;
                        string vol = string.Empty;
                        for (int i = sortedSevenCardsByNumer.Count - 1; i > 1; i--)
                        {
                            if (i != fourOfKindIndex && i != fourOfKindIndex + 1 && i != fourOfKindIndex + 2 && i != fourOfKindIndex + 3 && counter < 1)
                            {
                                counter++;
                                vol += (sortedSevenCardsByNumer[i].numer).ToString("D2");
                            }
                        }
                        potencialWinner.bestResult = long.Parse("8" + sortedSevenCardsByNumer[fourOfKindIndex].numer.ToString("D2") + vol + "000000");
                    }
                    else if (fullHouse)
                    {
                        potencialWinner.bestResult = long.Parse("7" + sortedSevenCardsByNumer[fullHouesIndex1].numer.ToString("D2") + sortedSevenCardsByNumer[fullHouesIndex2].numer.ToString("D2") + "000000");
                    }
                    else if (flush)
                    {
                        potencialWinner.bestResult = long.Parse("6" + (sortedSevenCardsByType[flushIndex].numer).ToString("D2") + "00000000");
                    }
                    else if (straight)
                    {
                        potencialWinner.bestResult = long.Parse("5" + (sortedSevenCardsByNumer[straightIndex].numer).ToString("D2") + "00000000");
                    }
                    else if (threeOfKind)
                    {
                        int counter = 0;
                        string vol = string.Empty;
                        for (int i = sortedSevenCardsByNumer.Count - 1; i > 1; i--)
                        {
                            if (i != threeOfKindIndex && i != threeOfKindIndex + 1 && i != threeOfKindIndex + 2 && counter < 2)
                            {
                                counter++;
                                vol += (sortedSevenCardsByNumer[i].numer).ToString("D2");
                            }
                        }
                        potencialWinner.bestResult = long.Parse("4" + sortedSevenCardsByNumer[threeOfKindIndex].numer.ToString("D2") + vol + "0000");
                    }
                    else if (twoPairs)
                    {
                        int counter = 0;
                        string vol = string.Empty;
                        for (int i = sortedSevenCardsByNumer.Count - 1; i > 1; i--)
                        {
                            if (i != twoParisIndex1 && i != twoParisIndex1 + 1 && i != twoParisIndex2 && i != twoParisIndex2 + 1 && counter < 1)
                            {
                                counter++;
                                vol += (sortedSevenCardsByNumer[i].numer).ToString("D2");
                            }
                        }
                        string pair1 = string.Empty;
                        string pair2 = string.Empty;
                        if (sortedSevenCardsByNumer[twoParisIndex1].numer > sortedSevenCardsByNumer[twoParisIndex2].numer)
                        {
                            pair1 = sortedSevenCardsByNumer[twoParisIndex1].numer.ToString("D2");
                            pair2 = sortedSevenCardsByNumer[twoParisIndex2].numer.ToString("D2");
                        }
                        else
                        {
                            pair1 = sortedSevenCardsByNumer[twoParisIndex2].numer.ToString("D2");
                            pair2 = sortedSevenCardsByNumer[twoParisIndex1].numer.ToString("D2");
                        }
                        potencialWinner.bestResult = long.Parse("3" + pair1 + pair2 + vol + "0000");
                    }
                    else if (pair)
                    {
                        int counter = 0;
                        string vol = string.Empty;
                        for (int i = sortedSevenCardsByNumer.Count - 1; i > 1; i--)
                        {
                            if (i != pairIndex && i != pairIndex + 1 && counter < 3)
                            {
                                counter++;
                                vol += (sortedSevenCardsByNumer[i].numer).ToString("D2");
                            }
                        }
                        potencialWinner.bestResult = long.Parse("2" + sortedSevenCardsByNumer[pairIndex].numer.ToString("D2") + vol + "00");
                    }
                    else
                    {
                        string val = string.Empty;
                        for (int i = sortedSevenCardsByNumer.Count - 1; i > 1; i--)
                        {
                            val += sortedSevenCardsByNumer[i].numer.ToString("D2");
                        }
                        potencialWinner.bestResult = long.Parse("1" + val);
                    }
                }
            }
        }
        void PlaceCardsOnTable(int amount)
        {
            deck.Last().gameObject.transform.position = new Vector3(stosCardsPosition.position.x, stosCardsPosition.position.y, -1);
            stosCards.Add(deck.Last());
            deck.RemoveAt(deck.Count - 1);
            for (int i = 0; i < amount; i++)
            {
                tableCards.Add(deck.Last());
                deck.RemoveAt(deck.Count - 1);
                tableCards.Last().gameObject.transform.position = new Vector3(tableCardsPositions[tableCards.Count - 1].position.x, tableCardsPositions[tableCards.Count - 1].position.y, -1);
                tableCards.Last().gameObject.transform.rotation = Quaternion.Euler(new Vector3(90f, 180f, 0f));
            }
        }
        void StandardRoundActions()
        {
            foreach (Player player in pokerController.players)
            {
                pul += player.pol;
                player.AddPolToRoundPol();
            }
            potText.text = pul.ToString();
            roundNumer++;
            actionsMade = 0;
            playerOnMoveIndex = dealer % activePlayers.Count;
            GiveToken();
        }
        bool IsGameOver()
        {
            int playersWithNoMoneyCounter = 0;
            foreach (Player player in pokerController.players)
            {
                if (player.money <= 0)
                {
                    playersWithNoMoneyCounter++;
                }
            }
            if (playersWithNoMoneyCounter == pokerController.players.Count - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public void OnButton_Pass_Clicked()
    {
        activePlayers.RemoveAt(playerOnMoveIndex);
        playerOnMoveIndex = playerOnMoveIndex % activePlayers.Count;
        databaseConnection.SendMove(pokerController.roomID, pokerController.myNickname, "Pass", string.Empty);
        mySelf.nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.gray;
        StandardButtonActions();
    }
    public void OnButton_Check_Clicked()
    {
        playerOnMoveIndex = (playerOnMoveIndex + 1) % activePlayers.Count;
        databaseConnection.SendMove(pokerController.roomID, pokerController.myNickname, "Check", string.Empty);
        mySelf.nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.yellow;
        StandardButtonActions();
    }
    public void OnButton_Call_Clicked()
    {
        int callValue = ToCall() - mySelf.pol;
        playerOnMoveIndex = (playerOnMoveIndex + 1) % activePlayers.Count;
        mySelf.AddMoneyToPul(callValue);
        databaseConnection.SendMove(pokerController.roomID, pokerController.myNickname, "Call", (callValue).ToString());
        mySelf.nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.yellow;
        StandardButtonActions();
    }
    public void OnButton_Bet_Clicked()
    {
        actionsMade = 0;
        actionsToMake = activePlayers.Count;
        playerOnMoveIndex = (playerOnMoveIndex + 1) % activePlayers.Count;
        mySelf.AddMoneyToPul(int.Parse(betText.text));
        databaseConnection.SendMove(pokerController.roomID, pokerController.myNickname, "Bet", betText.text);
        mySelf.nameKeeper.GetComponent<SpriteRenderer>().color = UnityEngine.Color.yellow;
        StandardButtonActions();
    }
    void StandardButtonActions()
    {
        actionWindow.SetActive(false);
        ResetButtons();
        LookForEndOfRound();
        stopEnteringMyTurn = false;
    }
    void ResetButtons()
    {
        check.interactable = true;
        call.interactable = true;
        bet.interactable = true;
        betSlider.value = 0;
        betSlider.minValue = 0;
        betSlider.maxValue = mySelf.money;
        betText.text = "0";
    }
    public void OnBetSliderValueChanged()
    {
        betText.text = ((int)betSlider.value).ToString();
    }
    public void OnButton_Ok_Clicked()
    {
        winnerPrefab.SetActive(false);
        menu.gameObject.SetActive(true);
        ResetData();
        lobby.ClearData();
        menu.ClearData();
        databaseConnection.removeRoom(pokerController.roomID);
        pokerController.ClearData();
    }
    public void ResetData()
    {
        foreach (Card card in deck)
        {
            Destroy(card.gameObject);
        }
        deck.Clear();
        foreach (Player player in pokerController.players)
        {
            player.PrepareDataForNewRound();
        }
        activePlayers.Clear();
        foreach (Card card in tableCards)
        {
            Destroy(card.gameObject);
        }
        tableCards.Clear();
        foreach (Card card in stosCards)
        {
            Destroy(card.gameObject);
        }
        stosCards.Clear();
        roundNumer = 0;
        stopEnteringMyTurn = false;
        stopEnteringOpponentTurn = false;
        mySelf = null;
        dealer = 0;
        pul = 0;
        isWaitingForDeck = false;
        playerOnMove = string.Empty;
        actionsMade = 0;
        gameBegan = false;

        actionWindow.SetActive(false);
        potText.text = "0";
        betSlider.value = 0;
        betSlider.maxValue = 1;
        betSlider.minValue = 0;
        betText.text = "0";
    }
}
