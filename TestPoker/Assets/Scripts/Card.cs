using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EType
{
    Club,
    Diamond,
    Heart,
    Spade
}

public class Card : MonoBehaviour
{
    public EType type;
    public int numer;
    public void AddValues(EType type, int numer)
    {
        this.type = type;
        this.numer = numer;
    }
}
