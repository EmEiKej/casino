using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] PokerController pokerController;
    [SerializeField] DatabaseConnection databaseConnection;
    [SerializeField] GameObject createRoom_JoinRoom;
    [SerializeField] GameObject createRoom;
    [SerializeField] GameObject joinRoom;
    [SerializeField] GameObject lobby;
    [SerializeField] TMP_InputField playerName;
    [SerializeField] TMP_InputField roomName;
    [SerializeField] TMP_InputField roomPassword;
    public void OnButton_CreateRoom_Clicked()
    {
        if (!InputFieldFormatCorrect(playerName.text))
        {
            return;
        }
        roomName.text = playerName.text;
        createRoom_JoinRoom.SetActive(true);
        createRoom.SetActive(true);
        this.gameObject.SetActive(false);
    }
    public void OnButton_JoinRoom_Clicked()
    {
        if (!InputFieldFormatCorrect(playerName.text))
        {
            return;
        }
        createRoom_JoinRoom.SetActive(true);
        joinRoom.SetActive(true);
        this.gameObject.SetActive(false);
    }
    public void On_CreateRoom_Button_CreateRoom_Clicked()
    {
        if (!InputFieldFormatCorrect(roomName.text) || !InputFieldFormatCorrect(roomPassword.text))
        {
            return;
        }
        foreach (var room in databaseConnection.GetRoomNames())
        {
            if (room == roomName.text)
            {
                return;
            }
        }
        databaseConnection.CreateRoom(roomName.text, roomPassword.text, playerName.text);
        pokerController.roomID = roomName.text;
        pokerController.myNickname = playerName.text;
        pokerController.isHost = true;
        lobby.SetActive(true);
        createRoom_JoinRoom.SetActive(false);
        createRoom.SetActive(false);
    }
    public void On_JoinRoom_Button_JoinRoom_Clicked()
    {
        if (!InputFieldFormatCorrect(roomName.text) || !InputFieldFormatCorrect(roomPassword.text))
        {
            return;
        }
        if (databaseConnection.CheckIfRoomIsRunnning(roomName.text))
        {
            return;
        }
        if (databaseConnection.GetPlayersNames(roomName.text).Count >= 8)
        {
            return;
        }
        foreach (var player in databaseConnection.GetPlayersNames(roomName.text))
        {
            if (player == playerName.text)
            {
                return;
            }
        }
        if (!databaseConnection.JoinRoom(roomName.text, roomPassword.text, playerName.text))
        {
            return;
        }
        pokerController.roomID = roomName.text;
        pokerController.myNickname = playerName.text;
        pokerController.isHost = false;
        lobby.SetActive(true);
        createRoom_JoinRoom.SetActive(false);
        joinRoom.SetActive(false);
    }
    public void OnButton_Back_Clicked()
    {
        this.gameObject.SetActive(true);
        createRoom_JoinRoom.SetActive(false);
        joinRoom.SetActive(false);
        createRoom.SetActive(false);
    }
    bool InputFieldFormatCorrect(string text)
    {
        if (text == string.Empty)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public void ClearData()
    {
        roomName.text = string.Empty;
        roomPassword.text = string.Empty;
        playerName.text = string.Empty;
    }
}
