using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Lobby : MonoBehaviour
{
    List<Vector2> positions = new List<Vector2>() { new Vector2(200, -400), new Vector2(-200, -400),
                              new Vector2(-600, -150), new Vector2(-600, 150),
                              new Vector2(-200, 400), new Vector2(200, 400),
                              new Vector2(600, 150), new Vector2(600, -150) };
    [SerializeField] PokerController pokerController;
    [SerializeField] DatabaseConnection databaseConnection;
    public List<GameObject> players = new List<GameObject>();
    [SerializeField] Button ready;
    [SerializeField] GameObject playerLobbyIcon;
    [SerializeField] Menu menu;
    [SerializeField] GameObject table;
    List<string> playerNames;
    bool GameIsRunning = false;
    void Start()
    {
        playerNames = databaseConnection.GetPlayersNames(pokerController.roomID);
        InitPlayers();
    }
    void InitPlayers()
    {
        foreach (string playerName in playerNames)
        {
            players.Add(Instantiate(playerLobbyIcon, this.gameObject.transform));
            players.Last().transform.localPosition = positions[players.Count - 1];
            players.Last().transform.GetChild(0).transform.GetChild(0).GetComponent<TMP_Text>().text = playerName;
            players.Last().GetComponent<Player>().GetName(playerName);
            pokerController.players.Add(players.Last().GetComponent<Player>());
        }
    }
    void CheckAmountOfPlayers()
    {
        if (GameIsRunning)
        {
            if (pokerController.players.Count < 2)
            {
                databaseConnection.removeRoom(pokerController.roomID);
                ClearData();
                pokerController.ClearData();
                menu.ClearData();
                menu.gameObject.SetActive(true);
                this.gameObject.SetActive(false);
            }
        }
        else
        {
            if (pokerController.players.Count >= 2)
            {
                ready.gameObject.SetActive(true);
            }
            else
            {
                ready.GetComponent<Image>().color = Color.red;
                ready.gameObject.SetActive(false);
                databaseConnection.PlayerReadyState(pokerController.roomID, pokerController.myNickname, false);
            }
        }
    }
    void AddPlayer()
    {
        players.Add(Instantiate(playerLobbyIcon, this.gameObject.transform));
        players.Last().transform.localPosition = positions[players.Count - 1];
        players.Last().transform.GetChild(0).transform.GetChild(0).GetComponent<TMP_Text>().text = playerNames.Last();
        players.Last().GetComponent<Player>().GetName(playerNames.Last());
        pokerController.players.Add(players.Last().GetComponent<Player>());
    }
    void UpdatePlayers()
    {
        if (databaseConnection.GetPlayersNames(pokerController.roomID).Count == 0)
        {
            ClearData();
            pokerController.ClearData();
            menu.ClearData();
            menu.gameObject.SetActive(true);
            this.gameObject.SetActive(false);

        }
        else if (databaseConnection.GetPlayersNames(pokerController.roomID).Count > playerNames.Count)
        {
            playerNames.Add(databaseConnection.GetPlayersNames(pokerController.roomID).Last());
            AddPlayer();
        }
        else if (databaseConnection.GetPlayersNames(pokerController.roomID).Count < playerNames.Count)
        {
            List<string> temptNames = databaseConnection.GetPlayersNames(pokerController.roomID);
            bool hisThere = false;
            for (int i = 0; i < players.Count; i++)
            {
                foreach (var name in temptNames)
                {
                    if (players[i].GetComponent<Player>().Name == name)
                    {
                        hisThere = true;
                    }
                }
                if (!hisThere)
                {
                    Destroy(players[i]);
                    players.RemoveAt(i);
                    pokerController.players.RemoveAt(i);
                    playerNames.RemoveAt(i);
                    for (int j = i; j < players.Count; j++)
                    {
                        players[j].transform.localPosition = positions[j];
                    }
                }
                else
                {
                    hisThere = false;
                }
            }
        }
    }
    bool CheckIfPlayersAreReady()
    {
        List<bool> states = new List<bool>();
        states = databaseConnection.GetPlayersState(pokerController.roomID);
        foreach (var state in states)
        {
            if (!state)
            {
                return false;
            }
        }
        return true;
    }
    [System.Obsolete]
    void Update()
    {
        CheckAmountOfPlayers();
        UpdatePlayers();
        if (ready.gameObject.active)
        {
            if (CheckIfPlayersAreReady())
            {
                databaseConnection.RoomIsRunning(pokerController.roomID);
                GameIsRunning = true;
                ready.GetComponent<Image>().color = Color.red;
                ready.gameObject.SetActive(false);
                table.SetActive(true);
            }
        }
    }
    public void ClearData()
    {
        playerNames.Clear();
        ready.GetComponent<Image>().color = Color.red;
        ready.gameObject.SetActive(false);
        foreach (var player in players)
        {
            Destroy(player);
        }
        players.Clear();
        GameIsRunning = false;
    }
    public void OnButton_Ready_Clicked()
    {
        if (ready.GetComponent<Image>().color == Color.green)
        {
            ready.GetComponent<Image>().color = Color.red;
            databaseConnection.PlayerReadyState(pokerController.roomID, pokerController.myNickname, false);
        }
        else
        {
            ready.GetComponent<Image>().color = Color.green;
            databaseConnection.PlayerReadyState(pokerController.roomID, pokerController.myNickname, true);
            if (CheckIfPlayersAreReady())
            {
                databaseConnection.RoomIsRunning(pokerController.roomID);
                GameIsRunning = true;
                ready.GetComponent<Image>().color = Color.red;
                ready.gameObject.SetActive(false);
                table.SetActive(true);
            }
        }
    }
}
