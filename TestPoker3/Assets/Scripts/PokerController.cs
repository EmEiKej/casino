using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PokerController : MonoBehaviour
{
    public string myNickname;
    public bool isHost;
    public string roomID;
    public List<Player> players = new List<Player>();
    [SerializeField] DatabaseConnection databaseConnection;
    [SerializeField] public List<Sprite> Tokens;
    public void ClearData()
    {
        myNickname = null;
        roomID = null;
        players.Clear();
    }
    private void OnApplicationQuit()
    {
        if (myNickname == roomID)
        {
            databaseConnection.removeRoom(roomID);
        }
        else
        {
            databaseConnection.removePlayer(roomID, myNickname);
        }
    }
}
