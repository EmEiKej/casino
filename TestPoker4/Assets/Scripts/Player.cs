using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class Player : MonoBehaviour
{
    public string Name;
    public List<Card> hand;
    [SerializeField] public Transform myCardSpot1;
    [SerializeField] public Transform myCardSpot2;
    [SerializeField] public GameObject token;
    [SerializeField] public TMP_Text moneyLabel;
    [SerializeField] public TMP_Text potLabel;
    [SerializeField] public GameObject nameKeeper;
    public int money = 5000;
    public int pol = 0;
    public int roundPol = 0;
    public long bestResult = 0;
    public void GetName(string Name)
    {
        this.Name = Name;
    }
    public void GetHand(List<Card> hand)
    {
        this.hand = hand;
    }
    public void AddMoneyToPul(int amount)
    {
        money -= amount;
        moneyLabel.text = money.ToString();
        pol += amount;
        potLabel.text = pol.ToString();
    }
    public void AddPolToRoundPol()
    {
        roundPol += pol;
        pol = 0;
        potLabel.text = pol.ToString();
    }
    public void RotateCards()
    {
        foreach (Card card in hand)
        {
            card.gameObject.transform.rotation = Quaternion.Euler(new Vector3(90f, -180f, 0f));
        }
    }
    public void PrepareDataForNewRound()
    {
        foreach (Card card in hand)
        {
            Destroy(card.gameObject);
        }
        hand.Clear();
        pol = 0;
        bestResult = 0;
        roundPol = 0;
        potLabel.text = pol.ToString();
        nameKeeper.GetComponent<SpriteRenderer>().color = Color.yellow;
    }
}
